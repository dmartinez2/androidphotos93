package com.example.androidphotos93;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class AllPhotosActivity extends AppCompatActivity {

    private static final String TAG = "AllPhotosActivity";

    //variables
    private TextView mTextMessage;
    private ArrayList<String> imageTitles = new ArrayList<>();
    private ArrayList<MyPhoto> images = new ArrayList<>();


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_photo:
                    //mTextMessage.setText(R.string.title_home);
                    break;
                case R.id.navigation_album:
                    //call all photos activity class
                    Intent intent = new Intent(AllPhotosActivity.this, AlbumActivity.class);
                    startActivity(intent);
                    break;
                case R.id.search:
                    Intent intent2 = new Intent(AllPhotosActivity.this, SearchActivity.class);
                    startActivity(intent2);
                    break;
            }
            return false;
        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_photos);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        //mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //set selected item on nav
        navView.getMenu().getItem(0).setChecked(true);

        //used for debugging
        Log.d(TAG, "onCreate: started. ");

        //call bitmaps only once per session
        initImageBitmaps();


    }

    @Override
    public void onResume() {
        super.onResume();
        Session.currentAlbum = null;
    }

    /**
     * this method gets all the image paths within the users internal storage
     */
    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps");
        //where to get the data
        final String[] columns = { MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID };
        final String orderBy = MediaStore.Images.Media._ID;
        //Stores all the images from the gallery in Cursor
        Cursor cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy);
        //Total number of images
        int count = cursor.getCount();
        Log.d(TAG,  "count: " + count);


        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            //Store the path of the image
            this.images.add(new MyPhoto(cursor.getString(dataColumnIndex)));
            //add to all photos as a bitmap
            Session.allPhotos.add(new MyPhoto(cursor.getString(dataColumnIndex)));
            Log.i("PATH", this.images.get(i).getPhoto_id());
        }
        // The cursor should be freed up after use with close()
        cursor.close();

        //call initRecyclerView
        initRecyclerView();

    }

    private void initRecyclerView(){
        //used for debugging
        Log.d(TAG, "initRecyclerView: init recyclerview.");

        //create recyclerview widget
        RecyclerView recyclerView = findViewById(R.id.allPhotoGrid);
        recyclerView.setHasFixedSize(true);
        //3 images per row
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this.images, this);
        recyclerView.setAdapter(adapter);
    }
}
