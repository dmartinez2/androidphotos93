package com.example.androidphotos93;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class AlbumViewAdapter extends RecyclerView.Adapter<AlbumViewAdapter.ViewHolder>{

    //used for debugging
    private static final String TAG = "AlbumViewAdapter";

    //variables
    private ArrayList<Album> mAlbums = Session.listOfAlbums;
    private Context mContext;


    /**
     * constructor for albumViewAdapter
     * @param mContext
     */
    public AlbumViewAdapter(ArrayList<Album> mAlbums, Context mContext) {
        this.mAlbums = mAlbums;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //used for debugging
        Log.d(TAG, "onCreateViewHolder: called.");

        //responsible for inflating the view with the album items
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_albumitem, viewGroup, false);
        //get the view holder
        ViewHolder holder =  new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        if (getItemCount() != 0) {
            //used for debugging
            Log.d(TAG, "onBindViewHolder: called.");
            //get the images for the album(not complete)
            if(!mAlbums.get(i).getPhotos().isEmpty()) {
                Glide.with(mContext)
                        .asBitmap()
                        .load(mAlbums.get(i).getPhotos().get(0).getPhoto_id())
                        .into(viewHolder.image);
            }



            //get album names
                viewHolder.name.setText(mAlbums.get(i).getAlbumName());

            //set up onclick listener

            viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //used for debugging
                    Log.d(TAG, "onClick: clicked on album: " + mAlbums.get(i).getAlbumName());
                    //display pop up for now
                    //Toast.makeText(mContext, mAlbums.get(i).getAlbumName(), Toast.LENGTH_SHORT).show();

                    Session.currentAlbum = mAlbums.get(i);
                    Intent intent = new Intent(v.getContext(), AlbumPhotoActivity.class);
                    v.getContext().startActivity(intent);
                }
            });

            viewHolder.parentLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(mContext, "Album selected", Toast.LENGTH_SHORT).show();
                    Session.currentAlbum = mAlbums.get(i);
                    return true;
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return mAlbums.size();
    }

    //holds view for recycler view
    public class ViewHolder extends RecyclerView.ViewHolder{

        //hold image and text
        ImageView image;
        TextView name;

        RelativeLayout parentLayout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //get the image and name location from layout
            image = itemView.findViewById(R.id.albumImage);
            name = itemView.findViewById(R.id.albumName);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
