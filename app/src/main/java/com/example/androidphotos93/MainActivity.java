package com.example.androidphotos93;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final int IMAGE_GALLERY_REQUEST = 20;
    private TextView mTextMessage;

    //dependencies
    private BottomNavigationView mMainNav;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_photo:
                    //call all photos activity class
                    Intent intent = new Intent(MainActivity.this, AllPhotosActivity.class);
                    startActivity(intent);
                    break;
                case R.id.navigation_album:
                    Intent intent2 = new Intent(MainActivity.this, AlbumActivity.class);
                    startActivity(intent2);
                    break;
                case R.id.search:
                    Intent intent3 = new Intent(MainActivity.this, SearchActivity.class);
                    startActivity(intent3);
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainNav = findViewById(R.id.nav_view);
        mMainNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //set selected item on nav
        mMainNav.getMenu().getItem(1).setChecked(true);

        //get user permission
        ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        //set the current album to null to prevent album-like activities
        Session.currentAlbum = null;

        //read the JSON
        try {
            Session.readJsonFile(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString());
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        //set main screen as all photos
        getPhotoLibrary();
    }

    public void getPhotoLibrary() {
        Intent intent = new Intent(this, AlbumActivity.class);
        finish();
        startActivity(intent);
    }


}
