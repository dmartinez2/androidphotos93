package com.example.androidphotos93;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SearchViewAdapter extends RecyclerView.Adapter<SearchViewAdapter.ViewHolder>{

    //used for debugging
    private static final String TAG = "SearchViewAdapter";

    //hold images
    private ArrayList<String> mImages = new ArrayList<>();

    //need context
    private Context mContext;

    //default constructor for recyclerView adapter


    public SearchViewAdapter(ArrayList<String> mImages, Context mContext) {
        this.mImages = mImages;
        this.mContext = mContext;
    }

    /**
     * this method puts the view items into the positions they should be put in to.
     * @param viewGroup
     * @param i
     * @return
     */
    @NonNull
    @Override
    public SearchViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d(TAG, "onCreateViewHolder: called");
        //responsible for inflating the view
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);
        //get the view holder
        SearchViewAdapter.ViewHolder holder =  new SearchViewAdapter.ViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull SearchViewAdapter.ViewHolder viewHolder, int i) {
        //used for debugging
        Log.d(TAG, "onBindViewHolder: called.");
        //get the images
        Glide.with(mContext)
                .asBitmap()
                .load(mImages.get(i))
                .into(viewHolder.image);

        //set up onclick listener

        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //used for debugging
                Log.d(TAG, "onClick: clicked on image: ");
                //display pop up for now
                Toast.makeText(mContext, "Image clicked!", Toast.LENGTH_SHORT).show();
            }
        });





    }

    /**
     * this method returns how many images we have
     * @return
     */
    @Override
    public int getItemCount() {
        return mImages.size();
    }

    //holds view for recycler view
    public class ViewHolder extends RecyclerView.ViewHolder{
        //hold image
        ImageView image;
        RelativeLayout parentLayout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //hold image location from layout
            image = itemView.findViewById(R.id.image);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
