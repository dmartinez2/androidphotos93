package com.example.androidphotos93;

import java.util.ArrayList;

public class MyPhoto {
    private String photoPath;
    //Note:We are choosing to do multple Location tags since the assignment
    //does NOT state it should be only one as it did with JavaFX
    private ArrayList<String> locationTags;
    private ArrayList<String> peopleTags;

    public MyPhoto(String photoPath) {
        this.photoPath = photoPath;
        this.locationTags = new ArrayList<String>();
        this.peopleTags = new ArrayList<String>();
    }

    public String getPhoto_id() {
        return this.photoPath;
    }

    public ArrayList<String> getLocationTags() { return this.locationTags; }

    public ArrayList<String> getPeopleTags () { return this.peopleTags; }

    public void setPhoto_id(String photoPath) {
        this.photoPath = photoPath;
    }

    public void setLocationTag(String locationTag) { this.locationTags.add(locationTag); }

    public void addPersonTag(String personTag) { this.peopleTags.add(personTag); }
}
