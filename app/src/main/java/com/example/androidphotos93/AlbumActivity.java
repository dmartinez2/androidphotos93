package com.example.androidphotos93;

import android.Manifest;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

public class AlbumActivity extends AppCompatActivity {


    private static final String TAG = "AlbumActivity";

    //variables
    private TextView mTextMessage;
    private ArrayList<Album> mAlbums = Session.listOfAlbums;
    private String albumTitle = "";


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_photo:
                    //call all photos activity class
                    Intent intent = new Intent(AlbumActivity.this, AllPhotosActivity.class);
                    startActivity(intent);
                    break;
                case R.id.navigation_album:
                    //Toast.makeText(getApplicationContext(),"album Clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.search:
                    Intent intent2 = new Intent(AlbumActivity.this, SearchActivity.class);
                    startActivity(intent2);
            }
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        //mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //set selected item on nav
        navView.getMenu().getItem(1).setChecked(true);

        //used for debugging
        Log.d(TAG, "onCreate: started. ");

        //null current photo
        Session.currentPhoto = null;

        //call bitmaps
        initImageBitmaps();
    }

    /**
     * this method gets all the image paths within the users internal storage
     */
    private void initImageBitmaps() {
        Log.d(TAG, "initImageBitmaps: preparing bitmaps");
        //where to get the data
        final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
        final String orderBy = MediaStore.Images.Media._ID;
        //Stores all the images from the gallery in Cursor
        Cursor cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy);
        //Total number of images
        int count = cursor.getCount();
        Log.d(TAG, "count: " + count);


        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            //Store the path of the image
            //this.imagePaths.add(cursor.getString(dataColumnIndex));
            //album name would go here, this is just used for testing
            //this.mNames.add("albumName" + i);
            //Log.i("PATH", this.imagePaths.get(i));
        }
        // The cursor should be freed up after use with close()
        cursor.close();

        //call initRecyclerView
        initRecyclerView();

    }

    private void initRecyclerView() {
        //used for debugging
        Log.d(TAG, "initRecyclerView: init recyclerview.");

        //create recyclerview widget
        RecyclerView recyclerView = findViewById(R.id.albumGrid);
        recyclerView.setHasFixedSize(true);
        //2 images per row
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        AlbumViewAdapter adapter = new AlbumViewAdapter(this.mAlbums, this);
        recyclerView.setAdapter(adapter);
    }

    /**
     * show the menu as 3 dots in the top right corner
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.albummenu, menu);
        return true;
    }

    /**
     * this will handle actions when the user clicks the button
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.albumCreate:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Provide Album Title");

                // Set up the input
                final EditText input = new EditText(this);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        albumTitle = input.getText().toString().trim();
                        //make sure input isnt null
                        if(albumTitle.length() == 0){
                            Toast.makeText(AlbumActivity.this, "Please enter an album name, try again.", Toast.LENGTH_SHORT).show();
                        } else{
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 0);

                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                break;
            case R.id.albumDelete:
                if (Session.currentAlbum == null) {
                    Toast.makeText(this, "No album selected. Long click an album to select.", Toast.LENGTH_LONG).show();
                } else {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                    builder2.setTitle("Delete Album?");

                    // Set up the buttons
                    builder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Session.listOfAlbums.remove(Session.currentAlbum);
                            Session.currentAlbum = null;
                            Session.makeJsonFile(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString());
                            finish();
                            startActivity(getIntent());
                        }
                    });
                    builder2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder2.show();
                    break;
                }
            case R.id.albumRename:
                if (Session.currentAlbum == null) {
                    Toast.makeText(this, "No album selected. Long click an album to select.", Toast.LENGTH_LONG).show();
                    break;
                } else {
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                    builder3.setTitle("Rename Album");

                    // Set up the input
                    final EditText input2 = new EditText(this);
                    // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                    input2.setInputType(InputType.TYPE_CLASS_TEXT);
                    builder3.setView(input2);

                    // Set up the buttons
                    builder3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Session.currentAlbum.setAlbumName(input2.getText().toString());
                            Session.makeJsonFile(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString());
                            finish();
                            startActivity(getIntent());
                        }
                    });
                    builder3.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder3.show();
                    break;
                }
        }
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ArrayList<MyPhoto> photoList = new ArrayList<>();
        ClipData clipdata = data.getClipData();

        if (null == data.getData()) {
            for (int i = 0; i < clipdata.getItemCount(); i++) {
                try {
                    //System.out.println("FAKE URI? " + clipdata.getItemAt(i).getUri().toString());
                    photoList.add(new MyPhoto(realPath(clipdata.getItemAt(i).getUri())));
                    //DO something
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } else {
            try {
                photoList.add(new MyPhoto(realPath(data.getData())));
                finish();
                startActivity(getIntent());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Session.listOfAlbums.add(new Album(photoList, albumTitle));
        Session.makeJsonFile(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString());

    }

    private String realPath(Uri uri) {
        // Will return "image:x*"
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = getContentResolver().
                query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);

        String filePath = "";

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }

        cursor.close();
        return filePath;
    }

    public void makeJsonFile() {
        Gson gson = new Gson();
        String json = gson.toJson(Session.listOfAlbums);
        //System.out.println("JSON FILE: " + json);

        File file = new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS) + "session.json");
        if (!file.exists()) {
            System.out.println("DIRS MADE");
            file.getParentFile().mkdirs();

            try {
                if (file.createNewFile()) {
                    System.out.println("YES, IT MADE THE FILE");
                } else {
                    System.out.println("NO, IT DID NOT MAKE THE FILE");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            PrintWriter output = new PrintWriter(new FileWriter(file));
            output.print(json);
            output.close();
        } catch (Exception IOException) {
            System.out.println(IOException);
            Toast.makeText(this, "Could not write JSON", Toast.LENGTH_SHORT).show();
        }
    }
}
