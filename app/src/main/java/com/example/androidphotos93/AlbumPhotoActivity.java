package com.example.androidphotos93;

import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AlbumPhotoActivity extends AppCompatActivity {

    private static final String TAG = "AlbumPhotoActivity";

    private ArrayList<MyPhoto> images = new ArrayList<>();


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_photo:
                    Intent intent1 = new Intent(AlbumPhotoActivity.this, AllPhotosActivity.class);
                    startActivity(intent1);
                    break;
                case R.id.navigation_album:
                    //call all photos activity class
                    Intent intent2 = new Intent(AlbumPhotoActivity.this, AlbumActivity.class);
                    startActivity(intent2);
                    break;
                case R.id.search:
                    Intent intent3 = new Intent(AlbumPhotoActivity.this, SearchActivity.class);
                    startActivity(intent3);
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_photos);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        //mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //set selected item on nav
        navView.getMenu().getItem(1).setChecked(true);

        //used for debugging
        Log.d(TAG, "onCreate: started. ");

        //call bitmaps
        initImageBitmaps();
    }

    /**
     * show the menu as 3 dots in the top right corner
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.album_modify_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.addPhoto:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 0);

                break;
            case R.id.removePhoto:
                if (Session.currentPhoto == null) {
                    Toast.makeText(this, "No photo selected. Long click a photo to select.", Toast.LENGTH_LONG).show();
                    break;
                } else {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                    builder2.setTitle("Delete Photo?");

                    // Set up the buttons
                    builder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Session.currentAlbum.getPhotos().remove(Session.currentPhoto);
                            Session.makeJsonFile(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString());
                            //null current photo
                            Session.currentPhoto = null;
                            finish();
                            startActivity(getIntent());
                        }
                    });
                    builder2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder2.show();
                    break;
                }
        }
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ClipData clipdata = data.getClipData();

        if (null == data.getData()) {
            for (int i = 0; i < clipdata.getItemCount(); i++) {
                try {
                    Session.currentAlbum.getPhotos().add(new MyPhoto(realPath(clipdata.getItemAt(i).getUri())));
                    finish();
                    startActivity(getIntent());
                    //DO something
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } else {
            try {
                Session.currentAlbum.getPhotos().add(new MyPhoto(realPath(data.getData())));
                finish();
                startActivity(getIntent());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Session.makeJsonFile(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString());
    }

    private String realPath(Uri uri) {
        // Will return "image:x*"
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = getContentResolver().
                query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);

        String filePath = "";

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }

        cursor.close();
        return filePath;
    }

    /**
     * this method gets all the image paths within the users internal storage
     */
    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps");

        images.clear();

        for (MyPhoto p : Session.currentAlbum.getPhotos()) {
            images.add(p);
        }

        //call initRecyclerView
        initRecyclerView();
    }

    private void initRecyclerView(){
        //used for debugging
        Log.d(TAG, "initRecyclerView: init recyclerview.");

        //create recyclerview widget
        RecyclerView recyclerView = findViewById(R.id.allPhotoGrid);
        recyclerView.setHasFixedSize(true);
        //3 images per row
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this.images, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        initImageBitmaps();
        initRecyclerView();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        initImageBitmaps();
        initRecyclerView();
    }
}
