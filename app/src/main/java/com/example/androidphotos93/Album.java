package com.example.androidphotos93;

import com.example.androidphotos93.MyPhoto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * This class handles all relevant functions for albums, including their photos and their names.
 * @author David Martinez
 * @author Justin Hinds <jth140>
 */
public class Album implements Serializable {
	/**
	 * The name of this album.
	 */
	private String albumName;
	
	/**
	 * The list of photos this album contains.
	 */
	private ArrayList<MyPhoto> photoList = new ArrayList<MyPhoto>();
	
	/**
	 * A constructor that initializes all fields of this object.
	 * @param photoList A list of photos that may initially be empty.
	 * @param name A string name.
	 */
	public Album(ArrayList<MyPhoto> photoList, String name) {
		this.photoList = photoList;
		this.albumName = name;
	}
	
	/**
	 * A method that returns the list of photos in this album.
	 * @return The list of photos in this album.
	 */
	public ArrayList<MyPhoto> getPhotos() {
		return this.photoList;
	}
	
	/**
	 * A method that adds a photo to this album and then serializes the user that owns this library.
	 * @param photo A photo to be added to this album.
	 */
	public void addPhoto(MyPhoto photo) {
		this.photoList.add(photo);
	}
	
	/**
	 * A method that removes a photo object from the list of photos and then serializes itself.
	 * @param photo A photo to be removed from this album.
	 */
	public void deletePhoto(MyPhoto photo) {
		if (this.photoList.contains(photo)) {
			this.photoList.remove(photo);
		}
	}
	
	/**
	 * A method that returns the name of this album.
	 * @return The name of this album.
	 */
	public String getAlbumName() {
		return this.albumName;
	}
	
	/**
	 * A method that sets the album name.
	 * @param albumName The new name to replace the old name of the album.
	 */
	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	
	/**
	 * A method which handles copying from one album to another
	 * provided that that album does not already have that photo in it.
	 * @param album The album to move to.
	 * @param photo The photo to move.
	 */
	public void copyPhotoTo(Album album, MyPhoto photo) {
		if (album.getPhotos().contains(photo)) {
			//do nothing, it's already in there
		} else {
			album.addPhoto(photo);
		}
	}
	
	/**
	 * A method which moves from one album to another, removing it from the original.
	 * @param album The album to which the photo is being moved.
	 * @param photo The photo being moved.
	 */
	public void movePhotoTo(Album album, MyPhoto photo) {
		this.deletePhoto(photo);
		copyPhotoTo(album, photo);
	}
	
	/**
	 * A method that returns the number of photos in the album.
	 * @return The number of photos in the album.
	 */
	public int getAlbumSize() {
		return this.getPhotos().size();
	}

}