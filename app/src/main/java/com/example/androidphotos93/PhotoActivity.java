package com.example.androidphotos93;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class PhotoActivity extends AppCompatActivity {


    private int selectedPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_imageslider);
        final ViewPager viewPager = findViewById(R.id.viewPager);
        ImageAdapter adapter = null;
        if (getIntent().getBooleanExtra("inAlbum", false)) {
            adapter = new ImageAdapter(this, Session.currentAlbum.getPhotos());
        } else {
            adapter = new ImageAdapter(this, Session.allPhotos);
        }
        viewPager.setAdapter(adapter);
        //this sets the image selected
        viewPager.setCurrentItem(getIntent().getIntExtra("index", 0));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if(Session.currentAlbum != null) {
                    Session.currentPhoto = Session.currentAlbum.getPhotos().get(i);
                    System.out.println("CURRENT POSITION: " + i);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        /*
        //mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);
        mUserImage = findViewById(R.id.fullscreen_content);

        mUserImage.setImageURI(Uri.parse(getIntent().getStringExtra("imagePath")));
        */

    }

    /**
     * show the menu as 3 dots in the top right corner
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        if (getIntent().getBooleanExtra("inAlbum", false)) {
            getMenuInflater().inflate(R.menu.photomenu, menu);
            return true;
        } else {
            return false;
        }

    }

    /**
     * this will handle actions when the user clicks the button
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch (id){
            case R.id.photoDetails:
                //2 types of tages
                String[] tagVals2 = {"location", "person"};
                //set up adapter for drop down menu
                final ArrayAdapter<String> adp = new ArrayAdapter<String>(PhotoActivity.this, R.layout.support_simple_spinner_dropdown_item, tagVals2);
                final Spinner sp = new Spinner(PhotoActivity.this);
                sp.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                sp.setAdapter(adp);

                //set up alert dialog
                AlertDialog.Builder builder2 = new AlertDialog.Builder(PhotoActivity.this);
                builder2.setTitle("Select a tag type");
                builder2.setView(sp);
                // Set up the buttons
                builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //set up another pop up window to accept a tag input

                        final String tagSelected = sp.getSelectedItem().toString();

                        AlertDialog.Builder builder3 = new AlertDialog.Builder(PhotoActivity.this);
                        builder3.setTitle(tagSelected + " tags");

                        final String[] peopleItems = Session.currentPhoto.getPeopleTags().toArray(new String[0]);
                        final String[] locationItems = Session.currentPhoto.getLocationTags().toArray(new String[0]);


                        if (tagSelected == "person") {
                            builder3.setItems(peopleItems, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                        } else {
                            builder3.setItems(locationItems, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                        }

                        // Set up the buttons
                        builder3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder3.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder3.show();


                    }
                });
                builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder2.create().show();
                break;
            case R.id.photoAddTag:
                //2 types of tages
                String[] tagVals = {"location", "person"};

                //set up adapter for drop down menu
               final ArrayAdapter<String> adp2 = new ArrayAdapter<String>(PhotoActivity.this, R.layout.support_simple_spinner_dropdown_item, tagVals );
               final Spinner sp2 = new Spinner(PhotoActivity.this);
               sp2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
               sp2.setAdapter(adp2);

                //set up alert dialog
               AlertDialog.Builder builder5 = new AlertDialog.Builder(PhotoActivity.this);
               builder5.setTitle("Select a tag type");
               builder5.setView(sp2);
                // Set up the buttons
                builder5.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //set up another pop up window to accept a tag input

                        final String tagSelected = sp2.getSelectedItem().toString();

                        AlertDialog.Builder builder3 = new AlertDialog.Builder(PhotoActivity.this);
                        builder3.setTitle("Add a new " + tagSelected + " tag");

                        // Set up the input
                        final EditText input = new EditText(PhotoActivity.this);

                        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                        input.setInputType(InputType.TYPE_CLASS_TEXT);
                        builder3.setView(input);

                        // Set up the buttons
                        builder3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //get tag entered
                                String tag = input.getText().toString().trim();
                                if(tag.length() == 0){
                                    Toast.makeText(PhotoActivity.this, "Please enter a tag, try again.", Toast.LENGTH_SHORT).show();
                                } else {
                                    //determine the value to place it in
                                    if(tagSelected.equals("location")){
                                        Session.currentPhoto.setLocationTag(tag);
                                    } else if(tagSelected.equals("person")) {
                                        Session.currentPhoto.addPersonTag(tag);
                                    }

                                    Session.makeJsonFile(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString());
                                    //used for debugging
                                    //System.out.println(Session.currentPhoto.getPeopleTags());
                                }



                            }
                        });
                        builder3.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder3.show();


                    }
                });
                builder5.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
               builder5.create().show();
                break;
            case R.id.photoDeleteTag:
                //display alert
                if (Session.currentPhoto.getLocationTags().isEmpty() && Session.currentPhoto.getPeopleTags().isEmpty()) {
                    Toast.makeText(this, "Photo has no tags.", Toast.LENGTH_SHORT).show();
                } else {

                    //find which tags are empty
                    boolean locationEmpty = false;
                    boolean peopleEmpty = false;
                    //get size of the tag array
                    int size = 0;
                    if(!Session.currentPhoto.getLocationTags().isEmpty()){

                        size += Session.currentPhoto.getLocationTags().size();
                    } else{
                        locationEmpty = true;
                    }
                    if(!Session.currentPhoto.getPeopleTags().isEmpty()){
                        size += Session.currentPhoto.getPeopleTags().size();
                    } else{
                        peopleEmpty = true;
                    }


                    //hold all current tags(needs to be final for lambda expression)
                    final String[] tags = new String[size];


                    //place location tags
                   if(!locationEmpty){

                       for (int i = 0; i < Session.currentPhoto.getLocationTags().size(); i++) {
                           tags[i] = Session.currentPhoto.getLocationTags().get(i);
                       }

                        if(!peopleEmpty){

                            int start = Session.currentPhoto.getLocationTags().size();

                            for (int j = start, k = 0; j < size; j++, k++){
                                tags[j] = Session.currentPhoto.getPeopleTags().get(k);
                            }

                        }


                   } else{
                       //place people tags
                       if(!peopleEmpty){
                           for (int i = 0; i < size; i++) {
                               tags[i] = Session.currentPhoto.getPeopleTags().get(i);
                           }
                       }
                   }

                    AlertDialog.Builder builder4 = new AlertDialog.Builder(PhotoActivity.this);
                    builder4.setTitle("Select a tag to delete");
                    builder4.setItems(tags, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            String tagToDelete = tags[which];

                            //find the value that holds the tag and remove it
                            if(Session.currentPhoto.getLocationTags().contains(tagToDelete)){
                                Session.currentPhoto.getLocationTags().remove(tagToDelete);
                            }
                            if(Session.currentPhoto.getPeopleTags().contains(tagToDelete)){
                                Session.currentPhoto.getPeopleTags().remove(tagToDelete);
                            }

                            Session.makeJsonFile(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString());

                            //used to debug
                            //System.out.println(Session.currentPhoto.getPeopleTags());


                        }
                    });
                    builder4.show();
                }

                break;
            case R.id.photoMove:
                AlertDialog.Builder builder = null;
                if (Session.currentAlbum == null) {
                    Toast.makeText(this, "This can only be done from an album.", Toast.LENGTH_SHORT).show();
                } else {
                    String[] album_names = new String[Session.listOfAlbums.size()];
                    for (int i = 0; i < Session.listOfAlbums.size(); i++) {
                        album_names[i] = Session.listOfAlbums.get(i).getAlbumName();
                    }

                    builder = new AlertDialog.Builder(this);
                    builder.setTitle("Pick album");
                    builder.setItems(album_names, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Session.currentAlbum.getPhotos().remove(Session.currentPhoto);
                            Session.listOfAlbums.get(which).getPhotos().add(Session.currentPhoto);
                            Session.makeJsonFile(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString());
                            finish();
                        }
                    });
                    builder.show();
                }
                break;
        }
        return true;
    }
}
