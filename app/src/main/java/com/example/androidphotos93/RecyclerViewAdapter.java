package com.example.androidphotos93;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    //used for debugging
    private static final String TAG = "RecyclerViewAdapter";

    //hold images
    private ArrayList<MyPhoto> mImages = new ArrayList<>();

    //need context
    private Context mContext;

    //default constructor for recyclerView adapter


    public RecyclerViewAdapter(ArrayList<MyPhoto> mImages, Context mContext) {
        this.mImages = mImages;
        this.mContext = mContext;
    }

    /**
     * this method puts the view items into the positions they should be put in to.
     * @param viewGroup
     * @param i
     * @return
     */
    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d(TAG, "onCreateViewHolder: called");
        //responsible for inflating the view
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);
         //get the view holder
        ViewHolder holder =  new ViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        //used for debugging
        Log.d(TAG, "onBindViewHolder: called.");
        //get the images
         Glide.with(mContext)
                 .asBitmap()
                 .load(mImages.get(i).getPhoto_id())
                 .into(viewHolder.image);

         //set up onclick listener

        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //used for debugging
                Log.d(TAG, "onClick: clicked on image: ");
                //display pop up for now
                //Toast.makeText(mContext, "Image clicked!", Toast.LENGTH_SHORT).show();
                String imagePath = mImages.get(i).getPhoto_id();
                Session.currentPhoto = mImages.get(i);
                Intent intent = new Intent(v.getContext(), PhotoActivity.class);
                intent.putExtra("index", i);
                intent.putExtra("inAlbum", Session.currentAlbum == null ? false : true);
                v.getContext().startActivity(intent);

                //pass the selected photo position
            }
        });

        viewHolder.parentLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(mContext, "Photo selected", Toast.LENGTH_SHORT).show();
                Session.currentPhoto = mImages.get(i);
                return true;
            }
        });
    }

    /**
     * this method returns how many images we have
     * @return
     */
    @Override
    public int getItemCount() {
        return mImages.size();
    }

    //holds view for recycler view
    public class ViewHolder extends RecyclerView.ViewHolder{
        //hold image
        ImageView image;
        RelativeLayout parentLayout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //hold image location from layout
            image = itemView.findViewById(R.id.image);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
