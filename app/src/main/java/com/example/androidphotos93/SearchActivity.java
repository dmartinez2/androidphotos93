package com.example.androidphotos93;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.support.v7.widget.SearchView;
import android.widget.Toast;
import android.app.SearchManager;
import android.widget.SearchView.OnQueryTextListener;


import java.util.ArrayList;



public class SearchActivity extends AppCompatActivity {

    private static final String TAG = "SearchActivity";

    //variables
    private TextView mTextMessage;
    private ArrayList<String> imageTitles = new ArrayList<>();
    private ArrayList<String> imagePaths = new ArrayList<>();
    private String tagValue;
    private RecyclerView recyclerView;
    private SearchViewAdapter adapter;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_photo:
                    //call all photos activity class
                    Intent intent = new Intent(SearchActivity.this, AllPhotosActivity.class);
                    startActivity(intent);
                    break;
                case R.id.navigation_album:
                    //call album activity class
                    Intent intent2 = new Intent(SearchActivity.this, AlbumActivity.class);
                    startActivity(intent2);
                    break;
                case R.id.search:
                    //Toast.makeText(getApplicationContext(), "search clicked!", Toast.LENGTH_SHORT).show();
                    break;
            }
            return false;
        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        //mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //set selected item on nav
        navView.getMenu().getItem(2).setChecked(true);

        //populate spinner
        String[] tagSpinner = new String[]{"location", "person", "both"};
        Spinner spinner = (Spinner)findViewById(R.id.searchSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, tagSpinner);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        //get tag value from the menu spinner
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                tagValue = (String)parent.getItemAtPosition(position);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //set initial position to location
                tagValue = (String)parent.getItemAtPosition(0);

            }
        });

        //get data from the search bar
        SearchView sv = findViewById(R.id.searchView);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                //do something when the user presses enter
                SearchTags(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                //do something on text change
                SearchTags(s);

                return false;
            }
        });

        //used for debugging
        Log.d(TAG, "onCreate: started. ");

        //call bitmaps(i dont think this is needed here)
        //initImageBitmaps();

    }

    /**
     * this method gets all the image paths within the users internal storage
     */
    private void initImageBitmaps(){
        /*
        Log.d(TAG, "initImageBitmaps: preparing bitmaps");
        //where to get the data
        final String[] columns = { MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID };
        final String orderBy = MediaStore.Images.Media._ID;
        //Stores all the images from the gallery in Cursor
        Cursor cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy);
        //Total number of images
        int count = cursor.getCount();
        Log.d(TAG,  "count: " + count);


        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            //Store the path of the image
            this.imagePaths.add(cursor.getString(dataColumnIndex));
            Log.i("PATH", this.imagePaths.get(i));
        }
        // The cursor should be freed up after use with close()
        cursor.close();
        */



    }

    private void initRecyclerView(){
        //used for debugging
        Log.d(TAG, "initRecyclerView: init recyclerview.");


        //set up recycler view
        recyclerView = findViewById(R.id.searchGrid);
        recyclerView.setHasFixedSize(true);
        //3 images per row
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new SearchViewAdapter(this.imagePaths, this);
        recyclerView.setAdapter(adapter);


    }

    //this method will be called to search tags, will place result into imagePaths
    private void SearchTags(String s){


        //empty image path
        this.imagePaths.clear();

        //hold tags
        ArrayList<String> tags = new ArrayList<String>();

        if(s.length() > 0) {



            //the cool way to do for loops
            for (Album a : Session.listOfAlbums) {
                for (MyPhoto p : a.getPhotos()) {
                    //this is for both
                    if(tagValue.equals("both")) {
                        if (!p.getPeopleTags().isEmpty()) {
                            tags.addAll(p.getPeopleTags());
                        }
                        if (!p.getLocationTags().isEmpty()) {
                            tags.addAll(p.getLocationTags());
                        }
                      //this is for location searches
                    } else if(tagValue.equals("location")){
                        if (!p.getLocationTags().isEmpty()) {
                            tags.addAll(p.getLocationTags());
                        }
                     //this is for people searches
                    } else if(tagValue.equals("person")){
                        if (!p.getPeopleTags().isEmpty()) {
                            tags.addAll(p.getPeopleTags());
                        }
                    }

                    //check each tag if it contains the current search
                    if (!tags.isEmpty()) {
                        for (String t : tags) {
                            if (t.contains(s)) {
                                //avoid multiple photos
                                if (!this.imagePaths.contains(p.getPhoto_id())) {
                                    this.imagePaths.add(p.getPhoto_id());
                                }
                            }
                        }
                    }
                    //clear tags
                    tags.clear();
                }
            }
        }

        initRecyclerView();

    }

}
