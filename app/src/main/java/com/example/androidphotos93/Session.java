package com.example.androidphotos93;

import android.graphics.Bitmap;
import android.os.Environment;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Session {
    public static ArrayList<Album> listOfAlbums = new ArrayList<>();
    public static ArrayList<MyPhoto> allPhotos = new ArrayList<>();
    public static Album currentAlbum;
    public static MyPhoto currentPhoto;

    public static void makeJsonFile(String envPath) {
        Gson gson = new Gson();
        String json = gson.toJson(Session.listOfAlbums);
        //System.out.println("JSON FILE: " + json);

        File file = new File(envPath + "session.json");
        if (!file.exists()) {
            System.out.println("DIRS MADE");
            file.getParentFile().mkdirs();

            try {
                if (file.createNewFile()) {
                    System.out.println("YES, IT MADE THE FILE");
                } else {
                    System.out.println("NO, IT DID NOT MAKE THE FILE");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            PrintWriter output = new PrintWriter(new FileWriter(file));
            output.print(json);
            output.close();
        } catch (Exception IOException) {
            System.out.println(IOException);
            System.out.println("COULD NOT WRITE JSON");
        }
    }

    public static void readJsonFile(String envPath) throws IOException {
        Gson gson = new Gson();
        String content = new String(Files.readAllBytes(Paths.get(envPath + "session.json")));
        Type collectionType = new TypeToken<ArrayList<Album>>(){}.getType();
        ArrayList<Album> albumList = gson.fromJson(content, collectionType);
        Session.listOfAlbums = albumList;
    }
}
